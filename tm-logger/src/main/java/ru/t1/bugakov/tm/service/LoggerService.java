package ru.t1.bugakov.tm.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.LinkedHashMap;
import java.util.Map;

public final class LoggerService {

    @NotNull private final ObjectMapper objectMapper = new YAMLMapper();

    public void log(@NotNull final String text) throws IOException {
        @NotNull final Map<String, Object> event = objectMapper.readValue(text, LinkedHashMap.class);
        @NotNull final String table = event.get("table").toString();
        final byte[] bytes = text.getBytes();
        @Nullable final File file = new File(table);
        if (!file.exists()) file.createNewFile();
        Files.write(Paths.get(table), bytes, StandardOpenOption.APPEND);
    }

}
