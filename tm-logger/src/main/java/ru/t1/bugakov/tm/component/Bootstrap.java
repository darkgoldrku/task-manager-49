package ru.t1.bugakov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.api.endpoint.*;
import ru.t1.bugakov.tm.api.service.IPropertyService;
import ru.t1.bugakov.tm.listener.EntityListener;
import ru.t1.bugakov.tm.service.LoggerService;
import ru.t1.bugakov.tm.service.PropertyService;
import ru.t1.bugakov.tm.util.SystemUtil;

import javax.jms.*;
import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;

public final class Bootstrap {
    @NotNull
    private static final String URL = ActiveMQConnection.DEFAULT_BROKER_URL;

    @NotNull
    private static final String QUEUE = "LOGGER";

    public void start() throws JMSException {
        @NotNull final LoggerService loggerService = new LoggerService();
        @NotNull final EntityListener entityListener = new EntityListener(loggerService);
        @NotNull final ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(URL);
        @NotNull final Connection connection = connectionFactory.createConnection();
        connection.start();
        @NotNull final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Queue destination = session.createQueue(QUEUE);
        @NotNull final MessageConsumer messageConsumer = session.createConsumer(destination);
        messageConsumer.setMessageListener(entityListener);
    }

}