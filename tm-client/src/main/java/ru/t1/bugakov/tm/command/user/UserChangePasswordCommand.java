package ru.t1.bugakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.dto.request.user.UserChangePasswordRequest;
import ru.t1.bugakov.tm.util.TerminalUtil;

import java.sql.SQLException;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    @Override
    public void execute() throws SQLException {
        System.out.println("[CHANGE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        getUserEndpoint().changeUserPassword(new UserChangePasswordRequest(getToken(), password));
    }

    @NotNull
    @Override
    public String getName() {
        return "user-change-password";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Change password of current user.";
    }

}
