package ru.t1.bugakov.tm.command;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.api.command.ICommand;
import ru.t1.bugakov.tm.api.service.IServiceLocator;

@Getter
@Setter
public abstract class AbstractCommand implements ICommand {

    @NotNull
    protected IServiceLocator serviceLocator;

    @NotNull
    @Override
    public String toString() {
        @NotNull final String name = getName();
        @Nullable final String argument = getArgument();
        @NotNull final String description = getDescription();
        @NotNull String result = "";
        if (!name.isEmpty()) result += name + " : ";
        if (argument != null && !argument.isEmpty()) result += argument + " : ";
        if (!description.isEmpty()) result += description;
        return result;
    }

    @Nullable
    protected String getToken() {
        return getServiceLocator().getTokenService().getToken();
    }

    protected void setToken(@Nullable final String token) {
        getServiceLocator().getTokenService().setToken(token);
    }

}
