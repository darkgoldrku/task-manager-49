package ru.t1.bugakov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.dto.request.data.DataJsonLoadFasterXmlRequest;

public final class DataJsonLoadFasterXmlCommand extends AbstractDataCommand {

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA LOAD JSON]");
        getDomainEndpoint().JsonLoadFasterXml(new DataJsonLoadFasterXmlRequest(getToken()));
    }

    @Override
    public @NotNull
    String getName() {
        return "data-load-json-fasterxml";
    }

    @Override
    public @NotNull
    String getDescription() {
        return "Load data from json file with fasterxml";
    }

}
