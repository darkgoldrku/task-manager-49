package ru.t1.bugakov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.bugakov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.bugakov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.bugakov.tm.api.endpoint.ITaskEndpoint;
import ru.t1.bugakov.tm.dto.model.ProjectDTO;
import ru.t1.bugakov.tm.dto.model.TaskDTO;
import ru.t1.bugakov.tm.dto.request.project.ProjectCreateRequest;
import ru.t1.bugakov.tm.dto.request.project.ProjectGetByIdRequest;
import ru.t1.bugakov.tm.dto.request.task.*;
import ru.t1.bugakov.tm.dto.request.user.UserLoginRequest;
import ru.t1.bugakov.tm.dto.response.task.TaskListResponse;
import ru.t1.bugakov.tm.enumerated.Status;
import ru.t1.bugakov.tm.marker.SoapCategory;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class TaskEndpointTest {

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance();

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance();

    @NotNull
    private final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance();

    @NotNull
    private List<TaskDTO> taskList;

    @NotNull
    private List<ProjectDTO> projectList;

    @Nullable
    private String token;

    @Before
    public void initEndpoint() throws Exception {
        taskList = new ArrayList<>();
        projectList = new ArrayList<>();
        token = authEndpoint.login(new UserLoginRequest("admin", "admin")).getToken();
        @Nullable final ProjectDTO project = projectEndpoint.createProject(new ProjectCreateRequest(token, "ClientTestProject", "ClientTestProjectDescription")).getProject();
        @Nullable final TaskDTO task = taskEndpoint.createTask(new TaskCreateRequest(token, "ClientTestTaskBind", "ClientTestTaskBind")).getTask();
        taskEndpoint.createTask(new TaskCreateRequest(token, "ClientTestTaskNotBind", "ClientTestTaskNotBind"));
        taskEndpoint.bindTaskToProject(new TaskBindToProjectRequest(token, project.getId(), task.getId()));
        taskList.add(taskEndpoint.getTaskById(new TaskGetByIdRequest(token, task.getId())).getTask());
        projectList.add(projectEndpoint.getProjectById(new ProjectGetByIdRequest(token, project.getId())).getProject());
    }

    @Test
    @Category(SoapCategory.class)
    public void testBindTaskToProject() throws Exception {
        @Nullable final String projectId = projectList.get(0).getId();
        Assert.assertEquals(1, taskEndpoint.listTasksByProjectId(new TaskListByProjectIdRequest(token, projectId)).getTasks().size());
        @Nullable final String taskId = taskEndpoint.createTask(new TaskCreateRequest(token, "ClientTestAnotherTaskBind", "ClientTestAnotherTaskBind")).getTask().getId();
        taskEndpoint.bindTaskToProject(new TaskBindToProjectRequest(token, projectId, taskId));
        Assert.assertEquals(2, taskEndpoint.listTasksByProjectId(new TaskListByProjectIdRequest(token, projectId)).getTasks().size());
    }

    @Test
    @Category(SoapCategory.class)
    public void testChangeTaskStatusById() throws SQLException {
        @NotNull final Status newStatus = Status.COMPLETED;
        @NotNull final TaskDTO taskForUpdate = taskList.get(0);
        taskEndpoint.changeTaskStatusById(new TaskChangeStatusByIdRequest(token, taskForUpdate.getId(), newStatus));
        @Nullable final TaskDTO taskAfterUpdate = taskEndpoint.getTaskById(new TaskGetByIdRequest(token, taskForUpdate.getId())).getTask();
        Assert.assertEquals(newStatus, taskAfterUpdate.getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void testClearTasks() throws SQLException {
        taskEndpoint.clearTasks(new TaskClearRequest(token));
        TaskListResponse response = taskEndpoint.listTasks(new TaskListRequest(token, null));
        Assert.assertNull(response.getTasks());
    }

    @Test
    @Category(SoapCategory.class)
    public void testCreateTask() throws Exception {
        int size = taskEndpoint.listTasks(new TaskListRequest(token, null)).getTasks().size();
        taskEndpoint.createTask(new TaskCreateRequest(token, "TestTaskAdd", "TestDescriptionAdd"));
        Assert.assertEquals(size + 1, taskEndpoint.listTasks(new TaskListRequest(token, null)).getTasks().size());
    }

    @Test
    @Category(SoapCategory.class)
    public void testGetTaskByIdPositive() throws SQLException {
        @NotNull final TaskDTO taskForSearch = taskList.get(0);
        Assert.assertNotNull(taskEndpoint.getTaskById(new TaskGetByIdRequest(token, taskForSearch.getId())));
        @Nullable final TaskDTO taskSearch = taskEndpoint.getTaskById(new TaskGetByIdRequest(token, taskForSearch.getId())).getTask();
        Assert.assertEquals(taskForSearch.getId(), taskSearch.getId());

    }

    @Test
    @Category(SoapCategory.class)
    public void testGetTaskByIdNegative() throws SQLException {
        @NotNull final String id = UUID.randomUUID().toString();
        Assert.assertNull(taskEndpoint.getTaskById(new TaskGetByIdRequest(token, id)).getTask());
    }

    @Test
    @Category(SoapCategory.class)
    public void testListTasksByProjectId() throws Exception {
        List<TaskDTO> tasks = taskEndpoint.listTasksByProjectId(new TaskListByProjectIdRequest(token, projectList.get(0).getId())).getTasks();
        Assert.assertEquals(tasks.size(), taskList.size());
    }

    @Test
    @Category(SoapCategory.class)
    public void testListTasks() throws SQLException {
        Assert.assertNotNull(taskEndpoint.listTasks(new TaskListRequest(token, null)).getTasks());
    }

    @Test
    @Category(SoapCategory.class)
    public void testRemoveTaskById() throws SQLException {
        int size = taskEndpoint.listTasks(new TaskListRequest(token, null)).getTasks().size();
        taskEndpoint.removeTaskById(new TaskRemoveByIdRequest(token, taskList.get(0).getId()));
        Assert.assertEquals(size - 1, taskEndpoint.listTasks(new TaskListRequest(token, null)).getTasks().size());
    }

    @Test
    @Category(SoapCategory.class)
    public void testUnbindTaskToProject() throws Exception {
        @Nullable final String projectId = projectList.get(0).getId();
        @Nullable final String taskId = taskList.get(0).getId();
        Assert.assertEquals(1, taskEndpoint.listTasksByProjectId(new TaskListByProjectIdRequest(token, projectId)).getTasks().size());
        taskEndpoint.unbindTaskToProject(new TaskUnbindFromProjectRequest(token, projectId, taskId));
        Assert.assertNull(taskEndpoint.listTasksByProjectId(new TaskListByProjectIdRequest(token, projectId)).getTasks());
    }

    @Test
    @Category(SoapCategory.class)
    public void testUpdateTaskById() throws SQLException {
        @NotNull final String newName = "TestTaskUpdate";
        @NotNull final String newDescription = "TestDescriptionUpdate";
        @NotNull final TaskDTO taskForUpdate = taskList.get(0);
        @Nullable final TaskDTO taskAfterUpdate = taskEndpoint.updateTaskById(new TaskUpdateByIdRequest(token, taskForUpdate.getId(), newName, newDescription)).getTask();
        Assert.assertEquals(newName, taskAfterUpdate.getName());
        Assert.assertEquals(newDescription, taskAfterUpdate.getDescription());
    }

}
