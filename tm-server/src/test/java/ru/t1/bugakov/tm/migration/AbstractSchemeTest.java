package ru.t1.bugakov.tm.migration;


import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.DatabaseException;
import liquibase.resource.ClassLoaderResourceAccessor;
import org.hsqldb.jdbcDriver;
import org.junit.BeforeClass;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public abstract class AbstractSchemeTest {

    private static final ClassLoaderResourceAccessor ACCESSOR = new ClassLoaderResourceAccessor();

    private static Database DATABASE;

    static {
        try {
            DriverManager.registerDriver(new jdbcDriver());
        } catch (Exception e) {
        }
    }

    @BeforeClass
    public static void beforeClass() throws IOException, DatabaseException, SQLException {
        final Properties properties = new Properties();
        final InputStream inputStream = ClassLoader.getSystemResourceAsStream("liquibase.properties");
        properties.load(inputStream);
        final Connection connection = getConnection(properties);
        final JdbcConnection jdbcConnection = new JdbcConnection(connection);
        DATABASE = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(jdbcConnection);
    }

    protected static Liquibase liquibase(final String fileName) {
        return new Liquibase(fileName, ACCESSOR, DATABASE);
    }

    private static Connection getConnection(Properties properties) throws SQLException {
        return DriverManager.getConnection(
                properties.getProperty("url"),
                properties.getProperty("username"),
                properties.getProperty("password")
        );
    }

}
