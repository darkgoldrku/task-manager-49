package ru.t1.bugakov.tm.migration;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.junit.Test;

public class ProjectSchemeTest extends AbstractSchemeTest {

    @Test
    public void test() throws LiquibaseException {
        final Liquibase liquibase = liquibase("liquibase/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("project");
    }
}
