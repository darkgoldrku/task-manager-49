package ru.t1.bugakov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.api.repository.model.IAbstractRepository;
import ru.t1.bugakov.tm.model.AbstractModel;

import javax.persistence.EntityManager;
import java.util.List;

public abstract class AbstractRepository<M extends AbstractModel> implements IAbstractRepository<M> {

    @NotNull
    protected final EntityManager entityManager;

    public AbstractRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void add(@NotNull final M entity) {
        entityManager.persist(entity);
    }

    @Override
    public void remove(@NotNull final M entity) {
        entityManager.remove(entity);
    }

    @Override
    public void update(@NotNull final M entity) {
        entityManager.merge(entity);
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return findById(id) != null;
    }

    @Override
    public void removeById(@NotNull String id) {
        @Nullable final M entity = findById(id);
        if (entity == null) return;
        remove(entity);
    }

    @Override
    public void clear() {
        @Nullable final List<M> models = findAll();
        if (models == null) return;
        for (@NotNull final M model : models) {
            remove(model);
        }
    }

    @Nullable
    @Override
    public abstract List<M> findAll();

    @Nullable
    @Override
    public abstract List<M> findAllWithSort(@Nullable final String sortField);

    @Nullable
    @Override
    public abstract M findById(@NotNull final String id);

    @Override
    public abstract int getSize();

}

