package ru.t1.bugakov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.dto.model.AbstractModelDTO;

import java.util.List;

public interface IAbstractDTORepository<E extends AbstractModelDTO> {

    void add(@NotNull final E model);

    List<E> findAll();

    List<E> findAllWithSort(@Nullable final String sortField);

    int getSize();

    E findById(@NotNull final String id);

    boolean existsById(@NotNull final String id);

    void update(E model);

    void clear();

    void remove(@NotNull final E model);

    void removeById(@NotNull final String id);

}
