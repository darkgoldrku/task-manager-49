package ru.t1.bugakov.tm.api.repository.model;

import ru.t1.bugakov.tm.model.Project;

public interface IProjectRepository extends IAbstractUserOwnedRepository<Project> {

}
