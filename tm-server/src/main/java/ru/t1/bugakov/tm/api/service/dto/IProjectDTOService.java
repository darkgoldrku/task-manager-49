package ru.t1.bugakov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.dto.model.ProjectDTO;
import ru.t1.bugakov.tm.enumerated.Status;

public interface IProjectDTOService extends IAbstractUserOwnedDTOService<ProjectDTO> {

    @NotNull
    ProjectDTO create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) throws Exception;

    @NotNull
    ProjectDTO updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description);

    @NotNull
    ProjectDTO changeProjectStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status);

}
