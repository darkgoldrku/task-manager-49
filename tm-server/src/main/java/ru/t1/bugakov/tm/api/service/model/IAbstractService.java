package ru.t1.bugakov.tm.api.service.model;

import ru.t1.bugakov.tm.api.repository.model.IAbstractRepository;
import ru.t1.bugakov.tm.model.AbstractModel;

public interface IAbstractService<M extends AbstractModel> extends IAbstractRepository<M> {

}
