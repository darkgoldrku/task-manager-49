package ru.t1.bugakov.tm.log;

public enum OperationType {

    INSERT,
    UPDATE,
    DELETE;

}

