package ru.t1.bugakov.tm.dto.response.task;

import lombok.NoArgsConstructor;
import ru.t1.bugakov.tm.dto.response.AbstractResponse;

@NoArgsConstructor
public final class TaskClearResponse extends AbstractResponse {
}
