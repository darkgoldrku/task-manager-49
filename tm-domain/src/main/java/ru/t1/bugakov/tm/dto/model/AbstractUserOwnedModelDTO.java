package ru.t1.bugakov.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.util.Date;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
public abstract class AbstractUserOwnedModelDTO extends AbstractModelDTO {

    @NotNull
    @Column(nullable = false, name = "user_id")
    private String userId;

    @NotNull
    @Column(nullable = false, name = "created")
    private Date created = new Date();

    public AbstractUserOwnedModelDTO(@NotNull String userId) {
        this.userId = userId;
    }

}
