package ru.t1.bugakov.tm.dto.response.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.dto.model.TaskDTO;

@NoArgsConstructor
public final class TaskBindToProjectResponse extends AbstractTaskResponse {

    public TaskBindToProjectResponse(@Nullable TaskDTO task) {
        super(task);
    }

}
