package ru.t1.bugakov.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
public enum SortType {

    BY_NAME("Sort by name"),
    BY_STATUS("Sort by status"),
    BY_CREATED("Sort by created");

    @NotNull
    private final String displayName;

    SortType(@NotNull String displayName) {
        this.displayName = displayName;
    }

    @Nullable
    public static SortType toSort(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        for (@NotNull final SortType sort : values()) {
            if (sort.name().equals(value)) return sort;
        }
        return null;
    }

}
