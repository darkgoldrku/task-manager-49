package ru.t1.bugakov.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.api.service.IConnectionProvider;
import ru.t1.bugakov.tm.dto.request.task.*;
import ru.t1.bugakov.tm.dto.response.task.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.sql.SQLException;

@WebService
public interface ITaskEndpoint extends IAbstractEndpoint {

    @NotNull
    String NAME = "TaskEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static ITaskEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static ITaskEndpoint newInstance(@NotNull final IConnectionProvider connectionProvider) {
        return IAbstractEndpoint.newInstance(connectionProvider, NAME, SPACE, PART, ITaskEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static ITaskEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return IAbstractEndpoint.newInstance(host, port, NAME, SPACE, PART, ITaskEndpoint.class);
    }

    @NotNull
    @WebMethod
    TaskBindToProjectResponse bindTaskToProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskBindToProjectRequest request
    ) throws SQLException;

    @NotNull
    @WebMethod
    TaskChangeStatusByIdResponse changeTaskStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskChangeStatusByIdRequest request
    ) throws SQLException;

    @NotNull
    @WebMethod
    TaskClearResponse clearTasks(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskClearRequest request
    ) throws SQLException;

    @NotNull
    @WebMethod
    TaskCreateResponse createTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskCreateRequest request
    ) throws Exception;

    @NotNull
    @WebMethod
    TaskGetByIdResponse getTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskGetByIdRequest request
    ) throws SQLException;

    @NotNull
    @WebMethod
    TaskListByProjectIdResponse listTasksByProjectId(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskListByProjectIdRequest request
    ) throws Exception;

    @NotNull
    @WebMethod
    TaskListResponse listTasks(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskListRequest request
    ) throws SQLException;

    @NotNull
    @WebMethod
    TaskRemoveByIdResponse removeTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskRemoveByIdRequest request
    ) throws SQLException;

    @NotNull
    @WebMethod
    TaskUnbindFromProjectResponse unbindTaskToProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskUnbindFromProjectRequest request
    ) throws SQLException;

    @NotNull
    @WebMethod
    TaskUpdateByIdResponse updateTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskUpdateByIdRequest request
    ) throws SQLException;

}
